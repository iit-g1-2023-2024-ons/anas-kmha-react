import React from 'react';
import { View } from 'react-native';
import CardPromo from '../../../components/exam/CardPromo';
 
const PromoCardMeta = {
  title: 'Promo Card ',
  component: CardPromo,
  argTypes: {
    onClickFavoris: { action: 'pressed the button' },
    orientation : {
      options: ['Vertical', 'Horizontal'],
      control: { type: 'radio' },
    }
  },
  args: {
    image: 'https://loremflickr.com/640/480',
    price: 1000,
    title: "promo 1",
    promotion: 50
  },
  decorators: [
    (Story) => (
      <View style={{ backgroundColor : "white" ,  alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Story />
      </View>
    ),
  ],
};

export default PromoCardMeta;

export const Horizontal = {
  args : {
    
    image: 'https://loremflickr.com/640/480',
    price: 10001,
    title: "Promo 1",
    
    onClickFavoris : () => console.log("test")
  }
 
};

export const Vertical = {
  args: {
    
    image: 'https://loremflickr.com/640/480',
    price: 10002,
    title: "Promo 2",
    
    onClickFavoris : () => console.log("test Vertical")
  },
};
